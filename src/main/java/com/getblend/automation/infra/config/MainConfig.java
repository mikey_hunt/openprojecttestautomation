package com.getblend.automation.infra.config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class MainConfig {

    public static String baseURL;
    public static String username;
    public static String password;

    public static void loadFromFile(String propertiesFilePath) throws Exception {

        InputStream input = new FileInputStream(propertiesFilePath);

        Properties properties = new Properties();

        properties.load(input);

        baseURL = properties.getProperty("baseURL");
        username = properties.getProperty("username");
        password = properties.getProperty("password");

    }
}
