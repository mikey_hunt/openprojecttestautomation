package com.getblend.automation.infra.report;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ConsoleReporter {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("[HH:mm:ss.SSS]: ");

    public static void log(String message) {
        System.out.println(sdf.format(new Date()) + message);
    }
}
