package com.getblend.automation.infra.web;

import com.getblend.automation.infra.report.ConsoleReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ActionBot {

    private WebDriver driver;

    public ActionBot(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement waitForElementToBeClickable(By elementBy, int timeoutInSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(timeoutInSeconds));
        return webDriverWait.until(ExpectedConditions.elementToBeClickable(elementBy));
    }
    public void click(By elementBy) {
        ConsoleReporter.log("Click on element " + elementBy);
        waitForElementToBeClickable(elementBy, 5).click();
    }

    public void writeToElement(By by, String text) {
        ConsoleReporter.log("Write '" + text + "' to element: " + by);
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(by));
        WebElement element = driver.findElement(by);
        element.clear();
        driver.findElement(by).sendKeys(text);
    }

    public String getElementText(By by) {
        String text = driver.findElement(by).getText();
        ConsoleReporter.log("Element " + by + " inner text is: " + text);
        return text;
    }

    public void waitForVisibilityOfElement(By by) {
        ConsoleReporter.log("Wait for visibility of element: " + by);
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }
}


