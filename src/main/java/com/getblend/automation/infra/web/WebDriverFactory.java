package com.getblend.automation.infra.web;

import com.getblend.automation.infra.enums.BrowserType;
import com.getblend.automation.infra.report.ConsoleReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;

public class WebDriverFactory {


    public static WebDriver getDriver(BrowserType driverType) {

        WebDriver driver = null;

        switch (driverType) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", "C:\\Users\\MikeHunt\\Documents\\Workspace\\chromedriver.exe");
                driver = new ChromeDriver();
                ConsoleReporter.log("Opened a new CHROME browser window");
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", "webdrivers/geckodriver.exe");
                driver = new FirefoxDriver();
                ConsoleReporter.log("Opened a new FIREFOX browser window");
                break;
            case EDGE:
                driver = new EdgeDriver();
                ConsoleReporter.log("Opened a new EDGE browser window");
                break;
        }

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.manage().window().maximize();

        return driver;
    }
}
