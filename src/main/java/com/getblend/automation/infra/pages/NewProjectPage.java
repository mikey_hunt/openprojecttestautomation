package com.getblend.automation.infra.pages;

import com.getblend.automation.infra.enums.ProjectStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

//This page is here http://localhost:8080/projects/new

public class NewProjectPage extends AbstractPage {

    private static final By nameInputElement = By.cssSelector("#formly_3_textInput_name_0");
    private static final By advancedSettingsDropDownElement = By.cssSelector(".op-fieldset--toggle");
    private static final By descriptionInputElement = By.cssSelector("#formly_9_formattableInput_description_1 div[aria-label='Rich Text Editor, main']");
    private static final By publicCheckboxElement = By.cssSelector("[type='checkbox']");
    private static final By statusDropDownElement = By.cssSelector("#formly_9_selectProjectStatusInput_status_4");
    private static final By onTrackInDropDownElement = By.cssSelector(".project-status--name.-on-track");
    private static final By atRiskInDropDownElement = By.cssSelector(".project-status--bulb.-inline.-at-risk");
    private static final By offTrackInDropDownElement = By.cssSelector(".project-status--name.-off-track");
    private static final By saveButtonElement = By.cssSelector("[type='submit']");
    private static final By memberPlusButton = By.cssSelector(".button.-alt-highlight");


    public NewProjectPage(WebDriver driver) {
        super(driver);
    }

    public NewProjectPage inputProjectName(String projectNameString) {
        bot.writeToElement(nameInputElement, projectNameString);
        return this;
    }

    public NewProjectPage clickAdvancedSettings() {
        bot.click(advancedSettingsDropDownElement);
        return this;
    }

    public NewProjectPage inputDescription(String description) {
        bot.writeToElement(descriptionInputElement, description);
        return this;
    }

    public NewProjectPage choosePublic() {
        bot.click(publicCheckboxElement);
        return this;
    }

    public NewProjectPage chooseStatus(ProjectStatus status) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.presenceOfElementLocated(statusDropDownElement));
        driver.findElement(statusDropDownElement).click();
        switch (status) {
            case ON_TRACK:
                driver.findElement(onTrackInDropDownElement).click();
                break;
            case AT_RISK:
                driver.findElement(atRiskInDropDownElement).click();
                break;
            case OFF_TRACK:
                driver.findElement(offTrackInDropDownElement).click();
                break;
        }
        return this;
    }

    public OverviewPage clickSaveButton() {
        bot.click(saveButtonElement);
        bot.waitForVisibilityOfElement(memberPlusButton);
        return new OverviewPage(driver);
    }
}