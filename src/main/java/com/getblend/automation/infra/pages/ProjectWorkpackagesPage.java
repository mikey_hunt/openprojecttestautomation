package com.getblend.automation.infra.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class ProjectWorkpackagesPage extends AbstractPage {

    private static final By subjectsTable = By.cssSelector(".wp-table--cell-td.__internal-sorthandle.wp-table--sort-td.hide-when-print");
    private static final By createButton = By.cssSelector(".button--icon.icon-add");
    private static final By taskButton = By.cssSelector(".menu-item.__hl_inline_type_1");
    private static final By inputTaskName = By.cssSelector("#wp-new-inline-edit--field-subject");
    private static final By inputDescription = By.cssSelector(".op-uc-p");
    private static final By saveNewTaskButton = By.cssSelector("#work-packages--edit-actions-save");
    private static final By closeTaskTab = By.cssSelector(".op-link.work-packages--details-close-icon");
    private static final By newTaskModal = By.cssSelector("[tabindex='-1'] .__hl_inline_type_1.inline-edit--display-field.type.-required.-editable");

    public ProjectWorkpackagesPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getSubjectsTable() {
        driver.navigate().refresh();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.presenceOfElementLocated(subjectsTable));
        List<WebElement> workPackagesList = (List<WebElement>) driver.findElements(By.cssSelector("[data-class-identifier]"));
        return workPackagesList;
    }

    public ProjectWorkpackagesPage createButtonClick() {
        bot.click(createButton);
        return this;
    }

    public ProjectWorkpackagesPage createNewTaskDropDownClick() {
        bot.click(taskButton);
        return this;
    }

    public ProjectWorkpackagesPage inputTaskName(String taskName) {
        bot.writeToElement(inputTaskName, taskName);
        return this;
    }

    public ProjectWorkpackagesPage inputTaskDescription(String taskDescription) {
        bot.writeToElement(inputDescription, taskDescription);
        return this;
    }

    public ProjectWorkpackagesPage saveNewTask() {
        bot.click(saveNewTaskButton);
        return this;
    }

    public ProjectWorkpackagesPage closeNewTask() {
        bot.click(closeTaskTab);
        return this;
    }

    public String getNewTaskModalName() {
        return bot.getElementText(newTaskModal);
    }
}
