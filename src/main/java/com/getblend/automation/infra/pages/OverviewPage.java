package com.getblend.automation.infra.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class OverviewPage extends AbstractPage {

    public OverviewPage(WebDriver driver) {
        super(driver);
    }

    private static final By projectNameElement = By.cssSelector(".op-app-menu--item-title.ellipsis");
    private static final By projectStatus = By.cssSelector(".project-status--name.-on-track");
    private static final By projectDescription = By.cssSelector(".op-uc-p");
    private static final By memberPlusButton = By.cssSelector(".button.-alt-highlight");
    private static final By mainMenuWorkPackages = By.cssSelector("#menu-sidebar a[id='main-menu-work-packages']");
    public static final By workPackagesButtonOnSidebarMenu = By.id("main-menu-work-packages");


    public String getProjectName() {
        String textOnButton = bot.getElementText(projectNameElement);
        return textOnButton;
    }

    public String getProjectStatus() {
        String textOnButton = bot.getElementText(projectStatus);
        return textOnButton;
    }

    public String getProjectDescription() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.presenceOfElementLocated(projectDescription));
        return driver.findElement(projectDescription).getText();
    }

    public OverviewPage choseWorkPackagesSection() {
        bot.click(workPackagesButtonOnSidebarMenu);
        return this;
    }


}
