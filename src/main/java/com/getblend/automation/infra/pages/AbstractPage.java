package com.getblend.automation.infra.pages;

import com.getblend.automation.infra.report.ConsoleReporter;
import com.getblend.automation.infra.web.ActionBot;
import org.openqa.selenium.WebDriver;

public class AbstractPage {
    protected WebDriver driver;
    protected ActionBot bot;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
        this.bot = new ActionBot(driver);
        ConsoleReporter.log("*** On page: " + this.getClass().getSimpleName());
    }
}
