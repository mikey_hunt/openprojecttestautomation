package com.getblend.automation.infra.pages;

import com.getblend.automation.infra.enums.QuickAddMenuTypes;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends AbstractPage{

    //Sign-in elements
    private static final By signInButton = By.cssSelector("[title='Sign in']");
    private static final By loginInputElement = By.cssSelector("[name='username']");
    private static final By passwordInputElement = By.cssSelector("[name='password']");
    private static final By signInPopUpButton = By.cssSelector("[name='login']");
    //Quick add menu elements
    private static final By openQuickAddMenu = By.cssSelector("[title='Open quick add menu']");
    private static final By projectQuickAddMenu = By.cssSelector("[title='New project']");
    private static final By inviteUserQuickAddMenu = By.cssSelector(".invite-user-menu-item.op-menu--item-action");
    //Porjects dropdown list
    private static final By openProjectsDropdownList = By.cssSelector(".op-app-menu.op-app-menu_drop-left");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage clickSignInDropDownButton() {
        bot.click(signInButton);
        return this;
    }

    public LoginPage typeUsername(String username) {
        bot.writeToElement(loginInputElement, username);
        return this;
    }

    public LoginPage typePassword(String password) {
        bot.writeToElement(passwordInputElement, password);
        return this;
    }

    public LoginPage clickSignButton() {
        bot.click(signInPopUpButton);
        return this;
    }

    public LoginPage openQuickAddMenu() {
       bot.click(openQuickAddMenu);
       return this;
    }

    public void chooseProjectFromQuickAddMenu(QuickAddMenuTypes parameter) {
        switch (parameter) {
            case PROJECT:
                driver.findElement(projectQuickAddMenu).click();
                break;
            case INVITE_USER:
                driver.findElement(inviteUserQuickAddMenu).click();
                break;
        }
    }

    public LoginPage clickOnProjectsList() {
        bot.click(openProjectsDropdownList);
        return this;
    }

    public LoginPage selectProjectFromProjectsList(String projectName){
        bot.click(By.cssSelector("[href='/projects/"+projectName+"']"));
        return this;
    }

}
