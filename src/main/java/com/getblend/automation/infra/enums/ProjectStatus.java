package com.getblend.automation.infra.enums;

public enum ProjectStatus {
    ON_TRACK ("ON TRACK"),
    AT_RISK("AT RISK"),
    OFF_TRACK("OFF TRACK");


    private final String value;

    private ProjectStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
