package com.getblend.automation.infra.enums;

public enum BrowserType {
    CHROME,
    FIREFOX,
    EDGE
}
