package com.getblend.automation.infra.enums;

public enum QuickAddMenuTypes {
    PROJECT,
    INVITE_USER,
    TASK,
    MILESTONE,
    PHASE,
    EPIC,
    USER_STORY,
    BUG
}
