package com.getblend.automation.tests;

import com.getblend.automation.infra.config.MainConfig;
import com.getblend.automation.infra.enums.ProjectStatus;
import com.getblend.automation.infra.enums.QuickAddMenuTypes;
import com.getblend.automation.infra.pages.LoginPage;
import com.getblend.automation.infra.pages.NewProjectPage;
import com.getblend.automation.infra.pages.OverviewPage;
import com.getblend.automation.infra.pages.ProjectWorkpackagesPage;
import com.getblend.automation.infra.report.ConsoleReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.UUID;

public class _001_OpenProjectFirstPage extends AbstractTestCase {

//    private static final String loginValue = "admin";
//    private static final String passwordValue = "Asdqwe123@";

    @Test
    public void signInTest() {
        //Step 1- login
        LoginPage loginPage = loginToOpenProject(MainConfig.username,MainConfig.password);
        driver.quit();
    }

    @Test
    public void createNewProjectTest() {
        //Step 1 - sign in
        LoginPage loginPage = loginToOpenProject(MainConfig.username,MainConfig.password);

        //Step 2 - On “Home” page, click “ + “ green button (top left corner), select “+ Project“ from the menu.
        loginPage.openQuickAddMenu().chooseProjectFromQuickAddMenu(QuickAddMenuTypes.PROJECT);

        //Step 3 - On the “New project” page, type a unique value for the project name.
        //Step 4 - Click “ADVANCED SETTINGS” title
        //Step 5 - Type some text to the description text box
        //Step 6 - Select “Public” checkbox
        //Step 7 - Select status “On track”
        //Step 8 - Click “Save”
        NewProjectPage newProjectPage = new NewProjectPage(driver);
        String UUID = java.util.UUID.randomUUID().toString();
        String description = "Description";

        OverviewPage overviewPage = new NewProjectPage(driver)
                .inputProjectName(UUID)
                .clickAdvancedSettings()
                .inputDescription(description)
                .choosePublic()
                .chooseStatus(ProjectStatus.ON_TRACK).clickSaveButton();

        //Test 9.1
        Assert.assertEquals(overviewPage.getProjectName(), UUID);
        System.out.println("Expected - " + overviewPage.getProjectName());
        System.out.println("Actual - " + UUID + "\n");

        //Test 9.2
        Assert.assertEquals(overviewPage.getProjectStatus(), ProjectStatus.ON_TRACK.getValue());
        System.out.println("Expected - " + overviewPage.getProjectStatus());
        System.out.println("Actual - " + ProjectStatus.ON_TRACK.getValue());

        //Test 9.3 //TODO this is waiting to be fixed by Rony
//       Assert.assertEquals(overviewPage.getProjectDescription(),description);

        driver.quit();
    }


    @Test
    public void createATaskInAProjectTest() {
        //Step 1- login
        //Step 2 - On “Home” page, top-left corner, click “Select a project“ menu button, and select “TestProject1” from the drop-down
        LoginPage loginPage = loginToOpenProject(MainConfig.username,MainConfig.password)
                .clickOnProjectsList()
                .selectProjectFromProjectsList("demo-project");

        //Step 3 - On the “Project Overview” page, left side menu, click “Work packages”.
        OverviewPage overviewPage = new OverviewPage(driver)
                .choseWorkPackagesSection();

        //Step 3.1 - Once on the “Work packages” page, note the number of rows displayed in the work packages table.
        ProjectWorkpackagesPage projectWorkpackagesPage = new ProjectWorkpackagesPage(driver);
        int amountOfRowsInTable = projectWorkpackagesPage.getSubjectsTable().size();

        //Step 4 - Click “+ Create” green button and select “TASK”
        //Step 6 - Type unique strings into the subject and description boxes
        String uniqueTaskName = UUID.randomUUID().toString();
        String uniqueTaskDescription = UUID.randomUUID().toString();
        ConsoleReporter.log("New task will have: name=" + uniqueTaskName + ", description=" + uniqueTaskDescription);

        projectWorkpackagesPage.createButtonClick()
                .createNewTaskDropDownClick()
                .inputTaskName(uniqueTaskName)
                .inputTaskDescription(uniqueTaskDescription);

        //Step 5 - 	Verify the text “New TASK” on top of the form that got opened on the right side
        String newTaskString = projectWorkpackagesPage.getNewTaskModalName();
        String newTask = "TASK";
        Assert.assertEquals(newTaskString, newTask);

        //Step 7 - 	Click “Save” button
        projectWorkpackagesPage.saveNewTask().closeNewTask();

        //Step 8 - Verify that a new row was added to the work packages table
        List<WebElement> rowsNew = projectWorkpackagesPage.getSubjectsTable();
        int amountOfRowsInTableAfterAddingTask = rowsNew.size();
        ConsoleReporter.log("Amount of tasks before test = " + amountOfRowsInTable + ". Amount of tasks after test - " + amountOfRowsInTableAfterAddingTask);
        Assert.assertEquals(amountOfRowsInTableAfterAddingTask, amountOfRowsInTable + 1);

        //Step 9 - Verify the subject and type of the last table row
        String lr = rowsNew.get(rowsNew.size() - 1).getText();
        System.out.println();
        System.out.println("LR - " + lr);

        System.out.println();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        List<WebElement> rowsNew1 = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//tr[@tabindex='0']")));
        int rowsCountNew = rowsNew1.size();
        WebElement lastRow = rowsNew1.get(rowsNew1.size() - 1);
        String lastRowText = lastRow.getText();
        System.out.println();
        System.out.println("lastRowText - " + lastRowText);
//        String lastRow = driver.findElement(By.cssSelector("tr[tabindex]:last-of-type")).getText();
//        System.out.println("1- " + driver.findElement(By.cssSelector("tr[tabindex]:last-of-type")).getText());
//        System.out.println("2- " + driver.findElement(By.cssSelector("tr>td>span>span.inline-edit--display-field.subject.-required.-editable:last-of-type")).getText());
//        System.out.println(lastRow.contains(unquieTaskName));
//
//        Assert.assertTrue(lastRow.contains(unquieTaskName));
//        Assert.assertTrue(lastRow.contains("TASK"));

        driver.quit();
    }

}
