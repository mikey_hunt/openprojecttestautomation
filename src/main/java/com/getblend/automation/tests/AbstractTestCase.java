package com.getblend.automation.tests;

import com.getblend.automation.infra.config.MainConfig;
import com.getblend.automation.infra.pages.LoginPage;
import com.getblend.automation.infra.report.ConsoleReporter;
import com.getblend.automation.infra.enums.BrowserType;
import com.getblend.automation.infra.web.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;

public abstract class AbstractTestCase {
    protected WebDriver driver;

    public void browseToUrl(String url) {
        ConsoleReporter.log("Browse to URL: " + url);
        driver.get(url);
    }

    @BeforeClass
    public void beforeClass() throws Exception {
            MainConfig.loadFromFile("config/MainConfig.properties");
    }

    public LoginPage loginToOpenProject(String username, String password) {

//        String baseUrl = "http://localhost:8080";

        driver = WebDriverFactory.getDriver(BrowserType.CHROME);
        browseToUrl(MainConfig.baseURL);

        LoginPage openProjectMainPage = new LoginPage(driver);
        openProjectMainPage
                .clickSignInDropDownButton()
                .typeUsername(username)
                .typePassword(password)
                .clickSignButton();

        return openProjectMainPage;
    }
}
